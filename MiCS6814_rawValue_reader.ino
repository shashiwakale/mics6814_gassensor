/*
 * MiCS6814_rawValue_reader.ino
 * 
 * This file contains source code to read raw values of MiCS6814 gas sensor for NH3 gas
 * Please refer datsheet of MiCS6814 for more information code is designed to operate on 
 * following conditions
 * 
 * VCC = 5V
 * Rs = 56K
 * ADC resolution = 1023 [10 Bit ADC]
 * 
 * PPM Range (For NH3 From Datasheet page no. 2 )
 * Ro = 10K for 1PPM  (ADC: 155) Analog Voltage: 757.58mV 
 * Ro = 1500K for 300PPM (ADC: 986) Analog Voltage: 4.82V
 * 
 * **************************ATTENTION*************************************
 * Sensor needs preheat at least 10 minutes before getting a stable data.
 * ************************************************************************
 * 
 * Author: Shashikant S. Wakale
 * Created on: 20-09-2018
 * Time: 21:12:00
 * Nashik
 * 
 */

 #define NH3_r A0       // ADC Pin for NH3
 #define lowBound 155   // Lower ADC reading bound calculated
 #define upperBound 986 // Upper ADC reading bound calculated
 #define lowPPM 1       // Lower PPM
 #define highPPM  300   // Upper PPM
  
 uint16_t analogValue = 0;  // Analog reading variable
 uint16_t ppmMappedValue = 0; // PPM Value Variable
 
void setup() {
 Serial.begin(9600);    // Set 9600 Buad Rate
}

void loop() {
  analogValue = analogRead(NH3_r);                      // Take ADC Data
  ppmMappedValue = map(analogValue, 155, 986, 1, 300);  // Map ADC Data to Desire PPM Range
  Serial.print("RAW VALUE: ");Serial.print(analogValue); Serial.print(" | ");       // Print Raw Value
  Serial.print("PPM VALUE: ");Serial.println(ppmMappedValue);   // Print PPM Value
  delay(1000);                                          // Delay 1 Sec
}
